#List of Cities and Municipalities in the Philippines (JSON Format)

##Description
This is a complete list of all the cities and municipalities in the country (Philippines) in JSON format, as derived from entries written in the wikipedia page.

Source: http://en.wikipedia.org/wiki/List_of_cities_and_municipalities_in_the_Philippines

##What Is This For
This is for people who needs to have a list of all the regions in the country to be used in their application project. It is conveniently divided into Provinces and Areas so that in a drop-down scenario, the user can select the provice and display the appropriate areas (Cities and Municipalities) in respect of that region.

##Why I Made This
Either my google skills failed me, or there is no resource for this particular problem. I have a project that needs the user to enter their intended location. I can't find an API for me to use, so I created this for the purpose. I'm sharing this to everyone so that people can update or check for accuracy.